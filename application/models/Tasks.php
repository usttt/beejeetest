<?php

class Tasks extends Model
{
    private $conn;
    private $pdo;

    public function __construct()
    {
        $conn = 'mysql:host=127.0.0.1;dbname=beejee;';
        $pdo = new \PDO($conn, 'root', '');
        $this->conn = $conn;
        $this->pdo = $pdo;
    }

    public function getData()
    {
        $sql = 'SELECT * FROM tasks';

        return $this->pdo->query($sql)->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function show($id)
    {
        $sql = "SELECT id, text, status FROM tasks WHERE id = '$id'";

        return $this->pdo->query($sql)->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function store($data)
    {
        $user_name = $data["user_name"];
        $email = $data['email'];
        $text = $data['text'];
        // SQL запрос на получение всех новостей
        $sql = "INSERT INTO tasks (user_name, email, text, status) 
                VALUES ('$user_name' , '$email' , '$text' , 0 )";

        return $this->pdo->query($sql);
    }

    public function update($data)
    {
        $status = $data['status'] == 'on' ? 1 : 0;
        $text = $data['text'];
        $id = $data['id'];

        $sql = "UPDATE tasks
                SET text = '$text', status = '$status'
                WHERE id = '$id'";

        return $this->pdo->query($sql);
    }
}