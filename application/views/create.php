<div class="container">
    <h1>Создать</h1>

    <form action="/store" method="post">
        <div class="form-group">
            <label>Имя пользователя</label>
            <input required name="user_name" type="text" class="form-control" placeholder="Имя пользователя">
        </div>

        <div class="form-group">
            <label>Email</label>
            <input required name="email" type="email" class="form-control"placeholder="Имя пользователя">
        </div>

        <div class="form-group">
            <label>Текст</label>
            <textarea required name="text" class="form-control" id="" cols="30" rows="10"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
</div>
