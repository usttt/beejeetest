<div class="container">
    <h1>Задачник</h1>

    <div class="d-flex justify-content-between">
        <a href="/create" class="btn btn-primary mb-2">Создать</a>
        <?php
        session_start();
        if(isset($_SESSION['admin'])){
            $admin = true;
            echo '<a href="/logout" class="btn btn-danger mb-2">Выйти</a>';
        }else{
            echo '<a href="/login" class="btn btn-secondary mb-2">Войти</a>';
        }
        ?>
    </div>


    <table class="table table-striped mt-4" id="myTable">
        <thead>
            <tr>
                <th>User name</th>
                <th>Email</th>
                <th>Text</th>
                <th>status</th>
            </tr>
        </thead>
        <tbody>
        <?php

        foreach ($data as $row) {

            $row['status'] == 1 ? $status = 'square-check' : $status = 'square-xmark';
            isset($admin) ? $edit = '<a href="/edit/'.$row['id'].'">Edit</a>' : $edit =  '';

            echo '<tr>
                <td>' . $row['user_name'] . '</td>
                <td>' . $row['email'] . '</td>
                <td>' . $row['text'] . '</td>
                <td> <i class="fa fa-'.$status.' " ></i> '.$edit.'</td>
                </tr>';
        }

        ?>
        </tbody>

    </table>
</div>
