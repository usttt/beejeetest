<div class="container">
    <h1>Редактировать</h1>

    <form action="/update" method="post">
        <input type="hidden" name="id" value="<?php echo $data[0]['id'];  ?>">
        <div class="form-group">
            <label>Текст</label>
            <textarea required name="text" class="form-control" id="" cols="30" rows="10"><?php echo $data[0]['text'];  ?></textarea>
        </div>

        <div class="form-group">
            <label>Статус</label>
            <input name="status" type="checkbox" <?php echo  $data[0]['status'] == 1 ? 'checked' : '';  ?> >
        </div>

        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
</div>
