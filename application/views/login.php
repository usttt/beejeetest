<div class="container">
    <h1>Войти</h1>

    <form action="/enter" method="post">

        <?php
        session_start();
        if(isset($_SESSION['error'])){
            echo '<div class="alert alert-danger" role="alert">'. $_SESSION['error']. '</div>';
            session_destroy();
        }
        ?>

        <div class="form-group">
            <label>Имя пользователя</label>
            <input name="name" type="text" class="form-control" placeholder="Имя пользователя">
        </div>

        <div class="form-group">
            <label>Пароль</label>
            <input name="password" type="password" class="form-control"placeholder="Пароль">
        </div>

        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
</div>
