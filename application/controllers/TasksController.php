<?php

class TasksController extends Controller
{
    public function __construct()
    {
        $this->model = new Tasks();
        $this->view = new View();
    }

    public function index()
    {
        $data = $this->model->getData();
        $this->view->generate('index.php', 'template.php', $data);
    }

    public function create()
    {
        $this->view->generate('create.php', 'template.php');
    }

    public function store()
    {
        $task = new Tasks();

        $task->store($_POST);

        header('Location: ' . '/');
    }

    public function edit($id)
    {
        $data = $this->model->show($id);

        $this->view->generate('edit.php', 'template.php', $data);
    }

    public function update()
    {
        $this->model->update($_POST);

        header('Location: ' . '/');
    }

    public function login()
    {
        $this->view->generate('login.php', 'template.php');
    }

    public function enter()
    {
        session_start();
        if($_POST['name'] == 'admin' && $_POST['password'] == '123' )
        {
            $_SESSION['admin'] = 'true';
            header('Location: ' . '/');
        }else{
            $_SESSION['error'] = 'Wrong username or password';
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

    public function logout()
    {
        session_start();
        session_destroy();
        header('Location: ' . '/');
    }
}